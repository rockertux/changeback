Gem::Specification.new do |s|
  s.name = "changeback"
  s.version = "1.0.0"
  s.date = "2018-07-22"
  s.summary = <<~DESC
    Calculate change for a set of bills in a cash register during a purchase
  DESC
  s.authors = ["Arthur Miranda"]
  s.email = ["ruby@tuxmiranda.com"]
  s.files = ["lib/changeback.rb", "lib/cash_register.rb"]
  s.homepage = "https://bitbucket.org/rockertux/changeback"
  s.license = "GPL-3.0"
end

# Changeback

Calculates due change for a purchase with a given set of bills handed to an
existing pool of bills, i.e., a cash register.

## Usage

1. Create a cash register

        #!ruby
        register = CashRegister.new(bills: { 10_00: 1, 5_00: 1 })

2. Perform calculation using created register, price and pile of bills from
customer

        #!ruby
        handed = { 20_00 => 1 }
        change = Changeback.(register, 15_00, handed) #=> { 5_00 => 1 }

    1. If change cannot be provided from current cash register, an error is
    raised with missing change remaining, change available to be handed back,
    and closest higher-valued available bill. Error message containing these
    data must be `eval`ed before use.

3. Update cash register if needed

        #!ruby
        register.insert(handed)
        register.remove(change)

### Notes

1. Operations in cash register and change calculations can be performed with
valid bills only. Providing non-existing bills will raise an `ArgumentError`.
2. All bill values are given in cents. For example, a $1 bill is represented
as `1_00`, while `1` is used for a $0.01 coin.
3. Valid bills are: $0.01, $0.05, $0.10, $0.25, $0.50, $1, $2, $5, $10, $20,
$50, $100. These are the currently issued bills and coins in Brazil.

## To-do

* Allow cash registers to use a user-defined set of bills

require "cash_register"

describe CashRegister do
  let(:provided_initial) do
    { 5 => 25, 1_00 => 10, 10_00 => 20 }
  end

  describe "initial state" do
    context "provided" do
      it "sets initial bill pool with given values, sets undeclared to 0" do
        register = CashRegister.new(bills: provided_initial)

        expect(register.state).to eq({
          1 => 0,
          5 => 25,
          10 => 0,
          25 => 0,
          50 => 0,
          1_00 => 10,
          2_00 => 0,
          5_00 => 0,
          10_00 => 20,
          20_00 => 0,
          50_00 => 0,
          100_00 => 0,
        })
      end

      context "with invalid bills" do
        it "raises argument error" do
          initial = provided_initial.merge({ 12 => 15 })
          expect {
            CashRegister.new(bills: initial)
          }.to raise_error(ArgumentError)
        end
      end
    end

    context "not provided" do
      it "initializes all bills with 0" do
        register = CashRegister.new

        expect(register.state).to eq({
          1 => 0,
          5 => 0,
          10 => 0,
          25 => 0,
          50 => 0,
          1_00 => 0,
          2_00 => 0,
          5_00 => 0,
          10_00 => 0,
          20_00 => 0,
          50_00 => 0,
          100_00 => 0,
        })
      end
    end
  end

  describe "#insert" do
    let(:incoming_bills) do
      { 2_00 => 3, 10_00 => 1 }
    end

    it "increments bills counters by given values" do
      register = CashRegister.new(bills: provided_initial)

      expect {
        register.insert(incoming_bills)
      }.to change(register, :state).to({
        1 => 0,
        5 => 25,
        10 => 0,
        25 => 0,
        50 => 0,
        1_00 => 10,
        2_00 => 3,
        5_00 => 0,
        10_00 => 21,
        20_00 => 0,
        50_00 => 0,
        100_00 => 0,
      })
    end

    context "invalid bills" do
      let(:invalid_input) do
        incoming_bills.merge({ 12 => 1 })
      end

      it "raises argument error" do
        register = CashRegister.new(bills: provided_initial)

        expect {
          register.insert(invalid_input)
        }.to raise_error(ArgumentError)
      end

      it "does not change bills pool" do
        register = CashRegister.new(bills: provided_initial)

        expect {
          register.insert(invalid_input) rescue ArgumentError
        }.not_to change(register, :state)
      end
    end
  end

  describe "#remove" do
    let(:outgoing_bills) do
      { 1_00 => 5, 10_00 => 1 }
    end

    it "decrements bills counters by given values" do
      register = CashRegister.new(bills: provided_initial)

      expect {
        register.remove(outgoing_bills)
      }.to change(register, :state).to({
        1 => 0,
        5 => 25,
        10 => 0,
        25 => 0,
        50 => 0,
        1_00 => 5,
        2_00 => 0,
        5_00 => 0,
        10_00 => 19,
        20_00 => 0,
        50_00 => 0,
        100_00 => 0,
      })
    end

    context "invalid bills" do
      let(:invalid_input) do
        outgoing_bills.merge({ 12 => 1 })
      end

      it "raises argument error" do
        register = CashRegister.new(bills: provided_initial)

        expect {
          register.remove(invalid_input)
        }.to raise_error(ArgumentError)
      end

      it "does not change bills pool" do
        register = CashRegister.new(bills: provided_initial)

        expect {
          register.remove(invalid_input) rescue ArgumentError
        }.not_to change(register, :state)
      end
    end

    context "remove more bills than available" do
      let(:excessive_withdrawal) do
        outgoing_bills.merge({ 1_00 => 50 })
      end

      it "raises argument error" do
        register = CashRegister.new(bills: provided_initial)

        expect {
          register.remove(excessive_withdrawal)
        }.to raise_error(ArgumentError)
      end

      it "does not update bills pool" do
        register = CashRegister.new(bills: provided_initial)

        expect {
          register.remove(excessive_withdrawal) rescue ArgumentError
        }.not_to change(register, :state)
      end
    end
  end
end

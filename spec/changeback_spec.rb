require "cash_register"
require "changeback"

describe Changeback do
  let(:cash_register) do
    register_state = {
      1 => 0,
      5 => 25,
      10 => 50,
      25 => 10,
      50 => 50,
      1_00 => 10,
      2_00 => 10,
      10_00 => 10,
      20_00 => 2
    }
    CashRegister.new(bills: register_state)
  end
  let(:handed) do
    { 50_00 => 1 }
  end
  let(:total) do
    23_00
  end

  describe ".call" do
    it "returns highest possible bills from cash register for given total and handed bills" do
      expected_change = {
        20_00 => 1,
        2_00 => 3,
        1_00 => 1,
      }
      returned_change = Changeback.(cash_register, total, handed)

      expect(returned_change).to eq(expected_change)
    end

    it "does not update provided cash register" do
      expect {
        Changeback.(cash_register, total, handed)
      }.not_to change(cash_register, :state)
    end

    context "invalid set of handed bills" do
      it "raises argument error" do
        handed = { 23_00 => 1 }
        expect {
          Changeback.(cash_register, total, handed)
        }.to raise_error(ArgumentError)
      end
    end

    context "cannot return exact change" do
      it "raises error with available and missing change, higher bill" do
        cash_register = CashRegister.new(bills: { 20_00 => 1, 10_00 => 1 })

        expect{
          Changeback.(cash_register, total, handed)
        }.to raise_error(Changeback::MissingChange) do |error|
          expect(eval(error.message)).to eq({
            change_due: 7_00,
            higher: 10_00,
            available: { 20_00 => 1 }
          })
        end
      end

      context "higher bill not available" do
        it "raises error with available and missing change, nil higher bill"  do
          cash_register = CashRegister.new(bills: { 20_00 => 1 })

          expect{
            Changeback.(cash_register, total, handed)
          }.to raise_error(Changeback::MissingChange) do |error|
            expect(eval(error.message)).to eq({
              change_due: 7_00,
              higher: nil,
              available: { 20_00 => 1 }
            })
          end
        end
      end

      context "empty cash register" do
        it "raises error with missing change, nil higher bill, empty available change"  do
          cash_register = CashRegister.new(bills: {})

          expect{
            Changeback.(cash_register, total, handed)
          }.to raise_error(Changeback::MissingChange) do |error|
            expect(eval(error.message)).to eq({
              change_due: 27_00,
              higher: nil,
              available: {}
            })
          end
        end
      end
    end
  end
end

class Changeback
  class MissingChange < ArgumentError; end

  def initialize(cash_register, total_amount, handed_bills)
    @cash_register = CashRegister.new(bills: cash_register.state)
    @total_amount = total_amount
    @handed_bills = handed_bills

    validate_handed_bills!
  end

  def self.call(*params)
    new(*params).call
  end

  def call
    handed_total = handed_bills
      .map{ |bill, amount| bill * amount }
      .reduce(:+)
    ordered_bill_values = ordered_bills_asc.reverse
    change_due = handed_total - total_amount 

    change_bills = current_state.merge(current_state) { 0 }

    while change_due > 0 do
      current_bill = ordered_bill_values.first
      if current_bill
        if current_bill > change_due || current_state[current_bill] == 0
          ordered_bill_values.shift
          next
        end

        change_bills[current_bill] += 1
        cash_register.remove({ current_bill => 1 })
        change_due -= current_bill
      else
        raise MissingChange.new({
          change_due: change_due,
          higher: ordered_bills_asc.first,
          available: change_bills.reject(&zeroed),
        })
      end
    end

    change_bills.reject(&zeroed)
  end

  private

  attr_reader :cash_register, :total_amount, :handed_bills

  def validate_handed_bills!
    raise ArgumentError if (handed_bills.keys - current_state.keys).length > 0
  end

  def zeroed
    Proc.new{ |_, amount| amount == 0 }
  end

  def ordered_bills_asc
    current_state.reject(&zeroed).keys.sort
  end

  def current_state
    cash_register.state
  end
end

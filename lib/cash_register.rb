class CashRegister
  def initialize(bills: {})
    validate_bills!(bills)
    @state = initial_state.merge(bills)
  end

  def insert(bills)
    validate_bills!(bills)
    bills.each do |bill, count|
      state[bill] += count
    end
  end

  def remove(bills)
    current_state = state.dup
    insert(bills.merge!(bills) { |_, count| -1 * count })

    if invalid_state
      @state = current_state
      raise ArgumentError
    end
  end

  attr_reader :state

  private

  def invalid_state
    state.select{ |_, count| count < 0 }.length > 0
  end

  def validate_bills!(bills)
    raise ArgumentError if (bills.keys - initial_state.keys).length > 0
  end

  def initial_state
    {
      1 => 0,
      5 => 0,
      10 => 0,
      25 => 0,
      50 => 0,
      1_00 => 0,
      2_00 => 0,
      5_00 => 0,
      10_00 => 0,
      20_00 => 0,
      50_00 => 0,
      100_00 => 0,
    }
  end
end
